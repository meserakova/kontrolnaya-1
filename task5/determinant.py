def get_matrix_minor(m,i,j):
    '''вычленяет заданный минор матрицы'''
    return [row[:j] + row[j+1:] for row in (m[:i]+m[i+1:])]

def get_matrix_deternminant(m):
    '''находит определитель матрицы'''
    if len(m) == 2:
        return m[0][0]*m[1][1]-m[0][1]*m[1][0]

    determinant = 0
    for c in range(len(m)):
        determinant += ((-1)**c)*m[0][c]*get_matrix_deternminant(get_matrix_minor(m,0,c))
    return determinant