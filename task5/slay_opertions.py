def swap_rows(A, B, row1, row2):
    '''перемена местами двух строк системы'''
    A[row1], A[row2] = A[row2], A[row1]
    B[row1], B[row2] = B[row2], B[row1]


def divide_row(A, B, row, divider):
    '''деление строки системы на число'''
    A[row] = [a / divider for a in A[row]]
    B[row] /= divider


def combine_rows(A, B, row, source_row, weight):
    '''сложение строки системы с другой строкой, умноженной на число'''
    A[row] = [(a + k * weight) for a, k in zip(A[row], A[source_row])]
    B[row] += B[source_row] * weight
