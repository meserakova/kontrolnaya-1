s = 42
#--получаем иксы и вычисляем сумму
while True:
    try:
        x = int(input())
        s += 1 / x
    except EOFError:
        break
    
#--выводим результат
print(s)