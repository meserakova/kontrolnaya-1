import time
import graph as gh
import saxpy


def saxpy(a, x, y):
    z = [((x[0] * a) + y[0]), ((x[1] * a) + y[1])]
    return z 


def saxpy_perebor():
    z_int = []
    z_float = []
    for i in range(10**50, 10**60, 10**55):
        #--вычисляем целочисленный saxpy и засекаем время
        start_time = time.time()
        z = saxpy(i, [i//1.15, i//1.13], [i//0.33, i//0.55])
        stop_time = time.time()
        elapsed_time = stop_time - start_time
        #--находим длинну вектора
        z_lenght = (z[0]**2 + z_[1]**2) ** 0.5
        z_int.append([z_lenght, elapsed_time])

        #--вычисляем вещественный saxpy и засекаем время
        start_time = time.time()
        z = saxpy.saxpy(i, [i/1.15, i/1.13], [i/0.33, i/0.55])
        stop_time = time.time()
        elapsed_time = stop_time - start_time
        #--находим длинну вектора
        z = (z[0]**2 + z[1]**2) ** 0.5
        z_float.append([z_lenght, elapsed_time])
    return z_int, z_float


#--вычисляем saxpy
z_int, z_float = saxpy_perebor()
#--создаём график
gh_x_int, gh_y_int, gh_x_float, gh_y_float = gh.data_for_graph(z_int, z_float)
gh.graph(gh_x_int, gh_y_int, gh_x_float, gh_y_float)