import time
import get_matrix as g_m
import matrix_mult as m_m
import graph as gh


def matrix_perebor():
    m_int = []
    m_float = []
    for i in range(10, 100, 10):
        #--генирируем две целочисленные матрицы и засекаем время на их умножение
        m1 = g_m.matrix_int(i)
        m2 = g_m.matrix_int(i)     
        start_time = time.time()       
        m3 = m_m.matrix_multiplication(m1, m2)
        stop_time = time.time()
        elapsed_time = stop_time - start_time
        m_int.append([i, elapsed_time])

        #--генирируем две вещественные матрицы и засекаем время на их умножение
        m1 = g_m.matrix_float(i)
        m2 = g_m.matrix_float(i)     
        start_time = time.time()       
        m3 = m_m.matrix_multiplication(m1, m2)
        stop_time = time.time()
        elapsed_time = stop_time - start_time
        m_float.append([i, elapsed_time]) 

    return m_int, m_float


m_int, m_float = matrix_perebor()
#--создаём график
m_int_n, m_int_t, m_float_n, m_float_t = gh.data_for_graph(m_int, m_float)
gh.graph(m_int_n, m_int_t, m_float_n, m_float_t)